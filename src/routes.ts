import express from 'express';
import knex from './database/connection';

const routes = express.Router();

//Listar usuários
//GET - Essa é a requisição mais comum de todas. 
//Através dessa requisição nós pedimos a representação de um recurso: que pode ser um arquivo html, xml, json, etc.
routes.get('/users', async (request, response) => {
    const users = await knex ('users').select('*');

    return response.json(users);
});


//Inserir usuário
//POST - O método POST é utilizado quando queremos criar um recurso. 
//Quando usamos POST, os dados vão no corpo da requisição e não na URI.
routes.post('/users', async (request, response) => {
    const { name, email, phone } = request.body;

    await knex ('users').insert({
        name,  
        email,
        phone,
    });
    
    return response.json({ success: true });
});

//Listar um usuário
routes.get('/users/:id', async (request, response) => {
    const { id } = request.params;

    const user = await knex ('users').where('id', id).first();

    return response.json(user);
});

//Editar usuário
//PATCH - Serve para atualizar partes de um recurso, e não o recurso todo
routes.patch('/users/:id', async (request, response) => {
    const { id } = request.params;
    const { name, email, phone } = request.body;

    await knex('users').where('id', id).update({
        name,
        email,
        phone,
    });

    return response.json({ success: true });
});

//Deletar usuário
//Exclui o recurso especificado.
routes.delete('/users/:id', async (request, response) => {
    const { id } = request.params;

    await knex('users').where('id', id).delete();

    return response.json({ success: true });
});

export default routes;
